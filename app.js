const chalk = require('chalk')
const yargs = require('yargs')
const notes = require('./notes.js')

console.log(chalk.green.inverse.bold('Success'))

yargs.command({
    command: 'add',
    describe: 'Add a note!',
    builder: {
        title: {
            describe: 'Your Note Title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Your Note Content',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        // console.log('Title: ' + argv.title + ', ' + 'Content: ' + argv.body)
        notes.addNote(argv.title, argv.body)
    }
})

yargs.command({
    command: 'remove',
    describe: 'Remove a note!',
    builder: {
        title: {
            describer: 'Your note title',
            demandOption: true,
            type: 'string',

        }
    },
    handler(argv) {
        notes.removeNote(argv.title)
    }
})

yargs.command({
    command: 'list',
    describe: 'List all notes!',
    handler() {
        notes.listNotes()
    } 
})

yargs.command({
    command: 'read',
    describe: 'Read a Note!',
    builder: {
        title: {
            describe: 'Your note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        notes.readNote(argv.title)
    }
})

yargs.parse()
// console.log(notes.getNotes())