const fs = require('fs')
const chalk = require('chalk')

const listNotes = () => {
    const notes = loadNotes()
    console.log(chalk.blue.inverse('Your Notes:'))
    notes.forEach(note => console.log(note.title))
}

const readNote = (title) => {
    const notes = loadNotes()
    const note = notes.find(note => note.title === title)
    debugger
    if (note) {
        console.log(chalk.blue.inverse(note.title))
        console.log(note.body)
    } else {
        console.log(chalk.red.inverse('No note found!'))
    }
}

const addNote = (title, body) => {

    const notes = loadNotes()

    const duplicateNotes = notes.filter(note => note.title === title )

    if (duplicateNotes.length === 0) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('Note Added!'))
    } else {
        console.log(chalk.red.inverse('Note title already taken.'))
    }

}

const removeNote = (title) => {
    const notes = loadNotes()
    // noteFound = false
    const notesToKeep = notes.filter(note => note.title !== title)
    if (notes > notesToKeep) {
        saveNotes(notesToKeep)
        console.log(chalk.green.inverse(title + ' Note Removed!'))
    } else {
        console.log(chalk.red.inverse('No note found with the specified title!'))
    }
}

const loadNotes = () => {
    try {
        const bufferData = fs.readFileSync('notes.json')
        const JSONString = bufferData.toString()
        return JSON.parse(JSONString)
    } catch {
        return []
    }
}

const saveNotes = (notes) => {
    const notesString = JSON.stringify(notes)
    fs.writeFileSync('notes.json', notesString)
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}